<?php


class Youssuph_Splitorders_Model_Split
{

    const SHOULD_SPLIT_ORDER_COUNT = 1;
	const QUANTITY_ATTR_NAME = "qty_ordered"; 
	
	/**
	   * Separates the products in the $order into a 
	   * multi-dimensional array based on $attributeName
	   *
	   * @param object $order
	   * @param string $attributeName
	   */	
    public function splitOrder($order, $quote, $attributeName)
    {
        $items = $order->getAllVisibleItems();
		
		$splittedProducts = [];
		

		
        foreach ($items as $item)
        {
            $productId = $item->getProduct()->getId();
			
            $_product = Mage::getModel('catalog/product')->load($productId);

			//$attributeValue can be the value of a perfume brand (for example Gucci) or the name of a warehouse (for example Al-Qusais-Warehouse)
            $attributeValue = strtolower($_product->getData($attributeName));
			
			
			$qty = $item->getData(self::QUANTITY_ATTR_NAME);
			
            if (!empty($attributeValue)) {
				
				if(!isset($splittedProducts[$attributeValue])){
					
					$splittedProducts[$attributeValue] = [];					
				}
				  
				$splittedProducts[$attributeValue][] =  $item;
 
            }
        }

		if($this->orderIsHeterogeneous($splittedProducts)){
			$this->collateOrder($order, $splittedProducts);
		}

    }

	
	/**
	   * Initializes quote object and appends data for each sub-order
	   * which are grouped together based on $attributeName 
	   *
	   * @param object $order
	   * @param array $splittedProducts
	   */	
    private function collateOrder($order, $splittedProducts = [])
    {

		$quotes = [];
		
        foreach($splittedProducts as $splittedProduct)
        {

			$quote = $this->initQuote($order);		
			$quote = $this->setCustomerForQuote($quote, $order);
            $quote = $this->setProductForQuote($quote, $splittedProduct);			
			$quote = $this->setBillingShippingForQuote($quote, $order);			

	
            $quote->collectTotals()->save();
			
			$quotes[] = $quote;
        }

		$this->doSave($quotes, $order);

    }
	
	/**
	   * Saves all quotes in $quotes array. A transaction is used
	   * to ensure that the database is always in a consistent state
	   * after every write
	   *
	   * @param object $order
	   * @param array $quotes
	   *
	   */	
	private function doSave($quotes, $order){
		
        $parentOrderId = $order->getIncrementId();
	
		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		
		try{
			
			$connection->beginTransaction();
			
			foreach($quotes as $quote){
				
				$serviceQuote = Mage::getModel('sales/service_quote', $quote);
				
				$serviceQuote->submitAll();
			
				$serviceQuote->getOrder()
							->setRelationParentId($parentOrderId)
							->save();
						
				$quote->setIsActive(0)
					  ->save();
					  
				$this->emptyCart();					  
			}
			$connection->commit();
			
		}catch(Exception $e){
			 Mage::log("$ex->getMessage()", null, 'Youssuph_Splitorder_Errors.log');
			$connection->rollback();
		}

	}


	 /**
	   * Checks if there is more than one type of attributeName(examples are brands, warehouses) 
	   * in the order
	   *
	   * @param array $splittedProducts
	   *
	   * @return boolean
	   */	
	private function orderIsHeterogeneous($splittedProducts)
	{
		return count($splittedProducts) > self::SHOULD_SPLIT_ORDER_COUNT;
	}


	/**
	   * Empties the shopping cart.
	   * 
	   */	
	private function emptyCart()
	{			  
		Mage::getSingleton('checkout/cart')->truncate()->save();		
	}

	
	/**
	   * Instantiates the Quote Object.
	   *
	   * @param object $order
	   *
	   * @return object $quote
	   */	
	private function initQuote($order){
		
        $store = $order->getStore();

		return Mage::getModel('sales/quote')
				->setCurrency($order->AdjustmentAmount->currencyID)
				->setStoreId($store->getId());
	}
	
	
	
	/**
	   * Sets product data for the current quote.
	   *
	   * @param object $quote
	   * @param array $splittedProduct
	   *
	   * @return object $quote
	   */	
    private function setProductForQuote($quote, $splittedProduct)
    {
        foreach($splittedProduct as $key=>$item)
        {
			$product =$item->getProduct();
            $quote->addProduct($product, new Varien_Object($item->getProductOptions()));
        }
		
        return $quote;
    }
	
	
	/**
	   * Sets customer data for the current quote.
	   *
	   * @param object $quote
	   * @param object $order
	   *
	   * @return object $quote
	   */	
    private function setCustomerForQuote($quote, $order)
    {

        $customer = $order->getCustomer();
		$quote->assignCustomer($customer);
		
        return $quote;
    }

	
	
	/**
	   * Sets billing and shipping information for the current quote.
	   *
	   * @param object $quote
	   * @param object $order
	   *
	   * @return object $quote
	   */	
    private function setBillingShippingForQuote($quote, $order)
    {
        $billingAddress = $order->getBillingAddress()->getData();
        $shippingAddress = $order->getShippingAddress()->getData();				
		$shippingMethod = $order->getShippingMethod();
		$paymentMethod = $order->getPayment()->getMethodInstance()->getCode();
		
        $quote->getBillingAddress()->addData($billingAddress);	

        $quote->getShippingAddress()
			 ->addData($shippingAddress)
			 ->setCollectShippingRates(true)
			 ->collectShippingRates()
             ->setShippingMethod($shippingMethod)
             ->setPaymentMethod($paymentMethod);

        
        $quote->getPayment()->importData(array('method' => $paymentMethod));

        return $quote;
    }


}