<?php
class Youssuph_Splitorders_Model_splitOrderObserver
{
	const REGISTRY_NAME_PLACEHOLDER = "executed";
  
    public function splitOrder(Varien_Event_Observer $observer)
    {
        if (!Mage::registry(self::REGISTRY_NAME_PLACEHOLDER)) {
			
            $order = $observer->getEvent()->getOrder();			
			
            $splitterClass = Mage::getModel('splitorders/split');
            $splitterClass->splitOrder($order, 'product_brand');
			
            Mage::register(self::REGISTRY_NAME_PLACEHOLDER, true);
			
        }
    }
	
}